#!/usr/bin/env python3

import sys
import os

def transliterate_string(s):
    mapping = {
            "а": "a",
            "б": "b", 
            "в": "v",
            "г": "g",
            "д": "d",
            "е": "e",
            "ё": "e",
            "ж": "zh",
            "з": "z",
            "и": "i",
            "й": "i",
            "к": "k",
            "л": "l",
            "м": "m",
            "н": "n",
            "о": "o",
            "п": "p",
            "р": "r",
            "с": "s",
            "т": "t",
            "у": "u",
            "ф": "f",
            "х": "h",
            "ц": "ts",
            "ч": "ch",
            "ш": "sh",
            "щ": "sh",
            "ъ": "",
            "ы": "y",
            "ь": "",
            "э": "e",
            "ю": "iu",
            "я": "ia",
            "А": "a",
            "Б": "b",
            "В": "v",
            "Г": "g",
            "Д": "d",
            "Е": "e",
            "Ё": "e",
            "Ж": "zh",
            "З": "z",
            "И": "i",
            "Й": "i",
            "К": "k",
            "Л": "l",
            "М": "m",
            "Н": "n",
            "О": "o",
            "П": "p",
            "Р": "r",
            "С": "s",
            "Т": "t",
            "У": "u",
            "Ф": "f",
            "Х": "h",
            "Ц": "ts",
            "Ч": "ch",
            "Ш": "sh",
            "Щ": "sh",
            "Ъ": "", 
            "Ы": "y",
            "Ь": "", 
            "Э": "e",
            "Ю": "iu",
            "Я": "ia"
            }

    k = mapping.keys()
    result = ""

    for i in s:
        if i in k:
            result += mapping[i]
        else:
            result += i

    return result

def transliterate_hier(d):
    new_name = transliterate_string(d)
    os.renames(d, new_name)
    d = new_name
    if os.path.isdir(d):
        for e in os.listdir(path=d):
            transliterate_hier(d + '/' + e)

transliterate_hier(sys.argv[1])
